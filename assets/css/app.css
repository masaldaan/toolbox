/*
Site Styles
================================================================================

Framework
------------------------------------------------------------
We use the [Spectre css framework](https://picturepan2.github.io/spectre/getting-started.html) to provide a lightweight set of initial styles and common components.

This stylesheet's organization & naming conventions can be adapted when necessary to work with the framework.

Naming Convention
------------------------------------------------------------
We use the [Block-Element-Modifier (BEM) naming convention](http://getbem.com/introduction/). EX:

```
.menu {...}               // block
.menu__item {...}         // element
.menu__item--active {...} // modifier
```

It's ok to adopt Spectre's naming convention when extending their classes / components.  Please document this in a comment above the class, or in the component's section comment.

Organization
------------------------------------------------------------
Our styles are organized based on [ITCSS conventions](https://www.xfive.co/blog/itcss-scalable-maintainable-css-architecture/).  In particular the following sections are used:

**Elements** - Styles applied directly to html elements.

**Objects** - Undecorated design patterns.  These classes focus on structure and layout, are often containers, and can be used as a basis for different components.

**Components** - Specific UI components that focus on displaying content.  Components can be standalone, or implementations of objects.  Most of our styles will be components.

**Utilities** - Helper classes and overrides (trumps).  `!important` must go here, since it will override everything.

Spacing
------------------------------------------------------------
We use a 4px vertical rhythm, and Spectre sets the root to 20px.  So use multiples of .2 for normal spacing, and multiples of .1 for fine-grained spacing.

Colors
------------------------------------------------------------
Notes:
  - main = the primary color in each flavor
  - dark = use a light color to provide contrast (e.g. dark background / light foreground, and vice versa)

Primary: Dark Blue
  - #082B76 (dark)
  - #09328B (dark, main)
  - #4362A6 (dark)
  - #8B9FC8 (dark)
  - #C5CFE4
  - #E2E7F1 

Compliment: Light Blue
  - #8F97E5 (dark)
  - #9FA8FF (dark, main)
  - #B6BCFF (dark)
  - #CCD1FF (dark)
  - #E3E5FF
  - #EEF0FF

Compliment: Purple
  - #A284B0 (dark)
  - #B493C4 (dark, main)
  - #C6ACD2 (dark)
  - #D7C6E0 (dark)
  - #E9DFEE
  - #F2ECF5

Compliment: Pastel Yellow
  - #E5DBCE
  - #FFF3E5 (main)
  - #FFF6EB
  - #FFF9F1, rgba(255, 249, 241, 1)
  - #FFFBF7, rgba(255, 251, 247, 1)
  - #FFFDFA, rgba(255, 253, 250, 1)

Compliment: Cyan
  - #79D2E3
  - #87E9FC (main)
  - #A3EEFD
  - #BFF3FD
  - #D1F7FE
  - #EAFBFE
*/


/*
Elements
================================================================================
*/

/*
Typography
------------------------------------------------------------
*/
body {
  font-family: Quicksand, sans-serif;
}

h1, .h1,
h2, .h2,
h3, .h3,
h4, .h4,
h5, .h5,
h6, .h6 {
  color: #09328B;

  font-family: Lato, serif;
}

/* TODO: do the same for :first-child / margin-top? */
h1:last-child, .h1:last-child,
h2:last-child, .h2:last-child,
h3:last-child, .h3:last-child,
h4:last-child, .h4:last-child,
h5:last-child, .h5:last-child,
h6:last-child, .h6:last-child,
p:last-child,
ul:last-child,
ol:last-child,
dl:last-child {
  margin-bottom: 0;
}

p,
address {
  margin-bottom: 0.8rem;
}

ul {
  list-style: disc outside;
}

ol {
  list-style: decimal outside;
}

ul.list--unstyled,
ol.list--unstyled {
  margin: 0;

  list-style: none;
}

ul.list--with-actions,
ol.list--with-actions {
  margin: 0;
  list-style: none;
}

ul.list--with-actions li,
ol.list--with-actions li {
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
}

ul.list--horizontal,
ol.list--horizontal {
  margin-left: 0.8rem;
  margin-right: 0.8rem;
}

ul.list--horizontal > li,
ol.list--horizontal > li {
  display: inline-block;
  margin-top: 0;
  margin-left: 0.8rem;
}

ul.list--horizontal > li:first-child,
ol.list--horizontal > li:first-child {
  margin-left: 0;
}

dl {
  margin-top: 0;
}

dl.list--grid {
  display: grid;
  grid-template-columns: max-content auto;
}

dl.list--grid > dt {
  padding: 0 0.2rem 0.4rem 0;

  font-weight: normal;
  text-align: right;
}

dl.list--grid > dd {
  margin: 0;
  padding: 0 0 0.4rem 0.2rem;

  font-weight: bold;
}

dl.list--grid > dd:last-child {
  padding-bottom: 0;
}

a,
a:visited {
  border-bottom: 1px dotted #9FA8FF;

  color: #8F97E5;
}

a:focus,
a:hover,
a:active,
a.active {
  /* NOTE: Don't set border bottom size, so if the border is set to `none` by a more specific link style then it doesn't also have to specify that for the active case. */
  border-bottom-style: solid;
  border-bottom-color: #8F97E5;

  color: #8F97E5;

  text-decoration: none;
}

a.link--wrapper {
  border-bottom: none;
}

a.link--unstyled,
a.link--unstyled:visited,
a.link--unstyled:focus,
a.link--unstyled:hover,
a.link--unstyled:active,
a.link--unstyled.active {
  border-bottom: none;
  box-shadow: none;

  color: #3B4351;
}

/*
Tables
------------------------------------------------------------
*/
.table th,
.table td {
  padding: 0.4rem;

  border-bottom-color: #E3E5FF;
}

.table.table-striped tbody tr:nth-of-type(2n+1) {
  background-color: #EEF0FF;
}

/*
Buttons
------------------------------------------------------------
*/
/* override default spectre colors */
.btn {
  box-sizing: border-box;

  color: #A284B0;
  border-color: #C6ACD2;
}
.btn:focus,
.btn:hover {
  background-color: #F2ECF5;
  border-color: #B493C4;
}
.btn:active,
.btn.active {
  background-color: #B493C4;
  border-color: #A284B0;
  color: #FFF;
}

/* override default spectre colors */
.btn.btn-primary {
  background-color: #C6ACD2;
  border-color: #B493C4;
  color: #FFF;

  font-family: Lato, serif;
  font-weight: bold;
}
.btn.btn-primary:focus,
.btn.btn-primary:hover {
  background-color: #B493C4;
  border-color: #A284B0;
}
.btn.btn-primary:active,
.btn.btn-primary.active {
  background-color: #A284B0;
  border-color: #A284B0;
}

/*
Form
------------------------------------------------------------
*/
.form-group:not(:last-child) {
  margin-bottom: 0.8rem;
}

.form-horizontal .form-group {
  align-items: center;
}

.form-group--required label {
  font-weight: bold;
}

.form-group--required label:after {
  content: "*";
  display: inline;

  color: #E32;

  font-weight: bold;
}

label {
  font-size: 0.8rem;
  text-transform: capitalize;
}

.form-horizontal label {
  width: 25%;
  padding-right: 0.8rem;

  text-align: right;
}

.form-horizontal .form-input,
.form-horizontal .form-select {
  width: 50%;
}

textarea.form-input {
  min-height: 100px;
}

.form-horizontal textarea.form-input {
  width: 75%;
}

.form-horizontal textarea.form-input--aligned {
  width: 50%;
}

.form-checkbox,
.form-radio,
.form-switch {
  min-height: 1.2rem;
  margin: 0;
  padding: 0;

  display: flex;
  align-items: center;
}

.form-horizontal label.form-checkbox,
.form-horizontal label.form-radio,
.form-horizontal label.form-switch {
  margin-left: 25%;
  padding-right: 0;

  text-align: left;
}

.form-checkbox .form-icon,
.form-radio .form-icon,
.form-switch .form-icon {
  position: relative;
  margin-right: 0.2rem;
}

.form-checkbox .form-icon,
.form-radio .form-icon {
  top: 0;
  width: 1.1rem;
  height: 1.1rem;
  padding: 0.1rem;  

  color: #FFF;
}

.form-input-hint {
  margin-bottom: 0;
}

.form-input-desc {
  margin-bottom: 0.8rem;

  color: rgba(17, 17, 19, 0.7);

  font-size: 0.7rem;
}

.form-group .form-input-desc {
  margin-top: 0.2rem;
  margin-bottom: 0;
}

.form-horizontal .form-input-hint,
.form-horizontal .form-input-desc {
  width: 75%;
  margin-left: auto;
}

fieldset {
  margin: 1.6rem 0;
  border-top: 1px solid rgba(17,17,19,.1);
  border-bottom: 1px solid rgba(17,17,19,.1);
  padding: 0.8rem 0;
}

legend {
  margin: 0 auto;
  padding: 0 0.8rem;

  font-family: Lato, serif;
  font-size: 1rem;
  font-weight: bold;
}

.form-horizontal legend {
  margin-left: calc(25% - 0.8rem); /* align w/ the inputs but leave room for the padding */
}

form .star {
  color: #E32;
  font-weight: bold;
}

/*
Icons
------------------------------------------------------------
Nothing here... for now. :)
*/

/*
Labels
------------------------------------------------------------
Nothing here... for now. :)
*/

/*
Media
------------------------------------------------------------
TODO:
 [ ] see how `display: block` affects group logos (especially centered ones)
*/
img {
  display: block;
  height: auto;
  max-width: 100%;
}


/*
Objects
================================================================================
*/

/*
Column
------------------------------------------------------------
Extend's Spectre.
*/
.columns--grid {
  margin-top: -0.4rem;
  margin-bottom: -0.4rem;
}

.columns--grid .column {
  padding-top: 0.4rem;
  padding-bottom: 0.4rem;
}

/*
Data Tables
------------------------------------------------------------
TODO:
  [ ] adjust for Spectre
*/
.dataTables_wrapper .dataTables_filter {
  float: none;
  margin-bottom: 0.3rem;

  text-align: left;
}

.dataTables_wrapper .dataTables_filter input {
  display: inline-block;
  width: 33.33333333%;
  margin-left: 0.8rem;
}

table.dataTable.no-footer {
  border-bottom: 1px solid #8F97E5;
}

table.dataTable thead th,
table.dataTable thead td {
  border-bottom: 1px solid #8F97E5;
}

table.dataTable thead th.sorting_disabled,
table.dataTable thead td.sorting_disabled {
  padding: 8px 10px;
}

table.dataTable thead .sorting,
table.dataTable thead .sorting_asc,
table.dataTable thead .sorting_desc,
table.dataTable thead .sorting_asc_disabled,
table.dataTable thead .sorting_desc_disabled {
  background-position: center left;
}

table.fixedHeader-floating {
  z-index: 100;
}

table.fixedHeader-floating.no-footer {
  border-bottom: 0;
}

/*
Navbar
------------------------------------------------------
TOOD:
  [ ] double check which styles are still needed for Spectre
  [ ] maybe move some of the color stuff to a component, tho it seems pretty minimal
*/
.navbar {
  position: fixed;
  top: 0;

  display: flex;
  align-items: center;
  width: 100%;
  height: 50px;
  box-shadow: 0 0.4rem 0.4rem 0 rgba(49, 52, 57, 0.2); /* #313439 */

  z-index: 1200;
}

.navbar a {
  border-bottom: none;
}

/*
Section
------------------------------------------------------
*/
.section {
  margin: 1.6rem;
}

.section__header {
  margin-bottom: 0.4rem;
}

.section__content {
  position: relative;
}

.section__footer {
  margin-top: 0.8rem;
}

.section--tabs {
  margin: 0rem 0.4rem 1.6rem 0.4rem;
}

.section--text {
  border-left: 0.1rem solid #CCD1FF;
  padding: 1.6rem;
}

.section--form {
  border-left: 0.1rem solid #CCD1FF;
  padding: 1.6rem 0;
}

.section--multi {}

.section--multi .section__header {}

.section--multi .section__content {
  margin-top: 1.6rem;
  margin-bottom: 1.6rem;
  border-left: 0.1rem solid #CCD1FF;
  padding: 0.8rem 0 0.8rem 0.8rem;
}

.section--multi .section__footer {}

.section--tabs .section__header {
  display: flex;
  align-items: flex-end;
  justify-content: space-between;

  margin-bottom: 0.8rem;
  border-bottom: 1px solid #E3E5FF;
}

.section--tabs .section__title {
  flex: 0 0 auto;
  margin: 0 0.4rem 0 0;
  border-bottom: 0.1rem solid #09328B;
  padding: 0 0.8rem;

  /*line-height: 1;*/
}

.section--tabs .section__action {
  align-self: center;
}

.section--tabs .section__nav {
  flex: 1 0 auto;

  font-size: 1rem;
}

.section--tabs .section__tab {
  margin-top: 0;
  border-bottom: 0;
}

.section--tabs .section__content {
  margin-left: 1.2rem;
  margin-right: 1.2rem;
}

.section--multi .section__content--thin,
.section--tabs .section__content--thin {
  margin-left: 0;
  margin-right: 0;
  border-left: none;
  padding: 0;
}

/*
Content Header
------------------------------------------------------------
*/
.content-header {
  display: flex;
  align-items: center;
  padding-bottom: 0.05rem;
  border-bottom: 0.05rem solid #CCD1FF;
  margin-bottom: 0.8rem;
}

.content-header:last-child {
  margin-bottom: 0.8rem;
}

.content-header--no-underline {
  padding-bottom: 0;
  border-bottom: none;
}

.content-header--centered {
  justify-content: center;
}

.content-header__title {
  flex: 1 0 0;
  margin: 0;
}

.content-header--centered .content-header__title {
  flex: 0 0 auto;
}

.content-header__title:not(:first-child) {
  margin-left: 0.4rem;
}


/*
Components
================================================================================
*/

/*
Accordion
------------------------------------------------------------
*/
.accordion .accordion-header .icon {
  cursor: pointer;
}

/*
Accordion Card
------------------------------------------------------------
Spectre mashup.
*/
.accordion.card .accordion-header {
  padding-bottom: 0.8rem;
}

.accordion.card .accordion-header .divider {
  display: none;
}

.accordion.card input:checked ~ .accordion-header,
.accordion.card[open] .accordion-header {
  padding-bottom: 0;
}

.accordion.card input:checked ~ .accordion-header .divider,
.accordion.card[open] .accordion-header .divider {
  display: block;
}

/*
Avatar
------------------------------------------------------------
Extend Spectre.

TODO:
  [ ] use it for map icons too
*/
.avatar {
  box-sizing: border-box;
  border: 0.05rem solid #EEF0FF;
}

a:focus > .avatar,
a:hover > .avatar,
a:active > .avatar,
a.active > .avatar {
  border-color: #8F97E5;
}

.avatar.avatar-xxl {
  width: 6.4rem;
  height: 6.4rem;
  border: 0.1rem solid #EEF0FF;  

  font-size: 3.2rem;
}

.avatar.avatar--filler {
  display: inline-flex;
  justify-content: center;
  align-items: center;

  background-color: #E3E5FF;
  border-color: #CCD1FF;
  color: #B6BCFF;

  font-size: 0.7rem;
}

/*
Action
------------------------------------------------------------
TODO:
  [ ] is this generic enough to be an Object?
  [ ] colors same as .btn.btn-primary.  should this be a "rounded button"?
*/
.actions {}

.actions--inline {
  margin-bottom: 0.8rem;
}

.actions--inline:last-child {
  margin-bottom: 0;
}

.action,
.action:visited {
  box-sizing: border-box;
  display: inline-flex;
  align-items: center;
  justify-content: center;

  width: 1.6rem;
  height: 1.6rem;
  border-radius: 100%;
  border: 1px solid #B493C4;

  color: #FFF;
  background-color: #C6ACD2;

  font-size: 0.8rem;
}

.action:focus,
.action:hover {
  background-color: #B493C4;
  border-color: #A284B0;
  color: #FFF;
}

.action:active,
.action.active {
  background-color: #A284B0;
  border-color: #A284B0;
  color: #FFF;
}


/*
Brand
------------------------------------------------------
TODO:
  [] only used in navbar- just override Spectre's .navbar-brand class instead
*/
.brand {
  padding: 0.2rem 0.4rem;

  color: inherit;

  font-family: Lato, serif;
  font-size: 1.2rem;
  text-transform: uppercase;
}

.brand:hover {
  text-decoration: none;
}

.brand > img {
  display: none;
}

/*
Card
------------------------------------------------------------
*/
/* TODO: Quick fix for simple cards on admin pages.  Might not be the best in a
         more complex case / in general overall.
*/
.card-header + .card-body {
  padding-top: 0.4rem;
}

/*
Category
------------------------------------------------------
TODO:
  [ ] only used on the homepage which will likely change significantly,
      probably want to remove
*/
.category {
  display: flex;
  flex-wrap: wrap;
  justify-content: space-evenly;
  align-items: flex-end;
  margin: 0;

  text-align: center;
}

.category > * {
  display: inline-block;
  flex-grow: 1;

  margin: 0.8rem;
}

a .category > * {
  border-bottom: none;
}

.category--inline > * {
  flex-grow: 0;
  margin: 0.8rem;
}

.category--2-col > * {
  flex: 0 0 auto;
  width: 50%;
  margin: 0.8rem 0;
}

.category--3-col > * {
  flex: 0 0 auto;
  width: 33.3%;
  margin: 0.8rem 0;
}

.category--4-col > * {
  flex: 0 0 auto;
  width: 25%;
  margin: 0.8rem 0;
}

.category__title {
  margin-bottom: 0;

  font-size: 1.12rem;
  font-weight: bold;
  text-align: center;
}

.category__icon {
  display: block;
  width: 3.2rem;
  height: 3.2rem;
  margin: 0 auto 0.4rem auto;

  font-size: 300%;
}

.category--inline .category__icon {
  margin: 0.4rem auto 0 auto;

  font-size: 250%;
}

.category__photo {
  display: block;
  width: 3.2rem;
  height: 3.2rem;
  margin: 0.4rem auto;
  border: 0.1rem solid #C8C8C8;
  border-radius: 100%;
}

/*
Checklist
------------------------------------------------------------
*/
ul.show_checklist li.checklist--checked {
  background: #888;
  color: #000000;
  text-decoration: line-through;
}
ul.show_checklist li.checklist--checked::before {
  content: '';
  position: absolute;
  border-color: #fff;
  border-style: solid;
  border-width: 0 0.1rem 0.1rem 0;
  top: 0.5rem;
  left: 0.8rem;
  transform: rotate(45deg);
  height: 0.75rem;
  width: 0.35rem;
}

.route_checklist {
  margin-left: 114px;
}

label + .route_checklist {
  margin-left: 0;
}

/*
Entry
------------------------------------------------------------
*/
.entry {
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  margin: 0.8rem 0;
}

.entry__title {
  padding-top: 6px; /* vertically align w/ .entry__actions */
}

.entry__actions {
  text-align: right;
}

.entry__content {
  flex-basis: 100%;
}

/*
Flash
------------------------------------------------------------
TODO:
  [ ] is this an object?
  [ ] aligned w/ layout so needs mobile interactive styling
*/
.flash {
  position: absolute;
  top: 0rem;
  left: 56.25%;
  width: 37.5%;
}

/*
Group Details
------------------------------------------------------------
*/
.group-details {
  display: inline-block;
  margin-left: 0.2rem;

  vertical-align: middle;
}

/*
Guidance Text
------------------------------------------------------------
*/
.guidance {
  color: #444;
  font-size: 0.7rem;
}

/*
Icons
------------------------------------------------------------
TODO:
  [ ] move this to icon styles in Elements, or extend Spectre's position utilities?
      https://picturepan2.github.io/spectre/utilities/position.html
*/
.is-icon-left {
  margin-right: 0.4rem;
}

.is-icon-right {
  margin-left: 0.4rem;
}

/*
Map
------------------------------------------------------
TODO:
  [ ] redo layout for this page
*/
.map-controls {
  width: 100%;
  font-size: 75%;
  padding: 4.8rem 0rem;
  margin-top: 0.8rem;
  background-color: rgba(255, 255, 255, 0.9);
}

.map {
  width: 100%;
  min-height: calc(100vh - 3.2rem);
  position: relative;
}

.map-icon--logo {
  width: 1.6rem;
  height: 1.6rem;
  margin-left: -0.8rem;
  border: 0.1rem solid #C8C8C8;
  border-radius: 100%;
}

/*
Panel
------------------------------------------------------------
*/
.form-horizontal .form-panel {
  width: 50%;
  margin-left: auto;
  margin-right: auto;
}

.form-panel {
  height: 50vh;
}

.form-panel label.form-switch {
  width: auto;
  margin-left: 0;
  align-items: start;
}

/*
Select2
------------------------------------------------------------
*/
.select2-selection__clear {
  font-size: 1.6rem;
  line-height: 1;

  color: #C6ACD2;
}

.select2-selection__clear:hover {
  color: #A284B0;
}

.select2-container .select2-selection--single {
  height: 1.8rem;
  border-radius: 0.1rem;
}
.select2-container--default .select2-selection--single .select2-selection__rendered {
  line-height: 1.8rem;
  vertical-align: middle;
}
.select2-container--default .select2-selection--single .select2-selection__arrow {
  height: 1.8rem;
}

.select2-container--default .select2-selection--multiple .select2-selection__choice {
  color: #3B4351;
  background-color: #F2ECF5;
  border-color: #B493C4;
}

.select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
  color: #C6ACD2;
}

.select2-container--default .select2-selection--multiple .select2-selection__choice__remove:hover {
  color: #A284B0;
}

/*
Shipment
------------------------------------------------------------
*/
.shipment {}

.shipment-details {}

/*
Shipment Roles
------------------------------------------------------------
*/
.role {
  margin-bottom: 0.8rem;
}

.role p {
  margin-bottom: 0;
}

/*
Shipment Routes
------------------------------------------------------------
*/
.route {
  margin-bottom: 0.8rem;
}

.route p {
  margin-bottom: 0;
}

/*
Site
------------------------------------------------------
*/
.site {
  background-color: #FFFDFA;
}

.site-header {
  position: sticky;
  top: 0;
  height: 100vh;
  overflow: hidden auto;
  border-right: 0.1rem solid #C5CFE4;

  background-color: #E2E7F1;
  color: #4362A6;
}

.site-header__logo {
  margin: 0.4rem 0.4rem 0.8rem 0.4rem;
}

.site-header__divider {
  margin: 0.4rem 0.8rem;
  border: none;
  border-bottom: 1px solid #C5CFE4;
  color: #C5CFE4;
}

.site-nav {
  font-family: Lato, serif;
  font-size: 0.9rem;
  list-style: none;
}

.site-nav--small {
  font-family: Quicksand, sans-serif;
  font-size: 0.7rem;
}

ul.site-nav {
  margin: 0;
}

ul.site-nav > li {
  margin-top: 0;
}

.site-nav a,
.site-nav a:visited {
  display: block;
  width: 100%;
  border-bottom: none;
  padding: 0.4rem;

  background-color: inherit;
  color: #4362A6;
}

.site-nav a:focus,
.site-nav a:hover,
.site-nav a:active,
.site-nav a.active {
  background-color: #C5CFE4;
  color: #082B76;
}

.site-content {}

/*
Subsection
------------------------------------------------------------
*/
.subsection {
  position: relative;
}

.subsection__header {
  margin: 1.6rem 0 0.4rem 0;
  border-bottom: 0.1rem dashed #313439;

  text-align: center;
}

/*
Tab
------------------------------------------------------------
*/
.tab {
  border-bottom-color: #E3E5FF;

  font-family: Lato, serif;
}

.tab .tab-item a {
  padding-left: 1.6rem;
  padding-right: 1.6rem;

  color: #B6BCFF;
}

.tab .tab-item a:focus,
.tab .tab-item a:hover {
  border-bottom-color: #E3E5FF;
  color: #9FA8FF;
}

.tab .tab-item a.active,
.tab .tab-item.active a {
  border-bottom-color: #B6BCFF;
  color: #8F97E5;
}

/*
Toast
------------------------------------------------------------
TOOD:
  [ ] double check that the spacing changes are still needed
  [ ] color
*/
.toast {
  margin-top: 0.8rem;
  margin-bottom: 0.8rem;
  padding: 0.4rem 0.8rem;
  border-radius: 0;

  font-size: 0.8rem;
}


/*
Utilities
==========================================================================
*/

/*
Backgrounds
------------------------------------------------------------
*/
.bg-img--cover {
  background: no-repeat top center fixed !important;
  background-size: cover !important;
  background-color: #C5CFE4; /* fallback if no image is used */
}

.bg-img--1 {
  background-image: url('/images/welcome-bg.jpg') !important;
}

.bg-color--1 {
  background-color: #4362A6 !important;
}

/*
Colors
------------------------------------------------------------
Nothing here... for now. :)
*/

/*
Text
------------------------------------------------------------
*/
.text-small {
  font-size: 87.5% !important;
}

.text-large {
  font-size: 112.5% !important;
}

.text-normal {
  font-weight: normal !important;
}

/*
Positioning
------------------------------------------------------------
TODO:
  [ ] should `flex-centered` be changed to use stretch instead?
*/
.flex-centered {
  height: 100%;
}

.flex-centered--col {
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
}

/*
Transforms
------------------------------------------------------------
*/
.flip--horizontal {
  transform: scaleX(-1) !important;
}

.flip--vertical {
  transform: scaleY(-1) !important;
}

.rotate--90 {
  transform: rotate(90deg) !important;
}

.rotate--180 {
  transform: rotate(180deg) !important;
}

.rotate--270 {
  transform: rotate(270deg) !important;
}
